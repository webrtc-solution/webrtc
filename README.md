# README #
Steps to compile webrtc

# Install#

apt-get install libssl-dev -y


apt install libsdl2-dev libsdl2-2.0-0 -y

apt install libfdk-aac-dev -y


cd /workspace

git clone git@github.com:FFmpeg/FFmpeg.git ffmpeg

cd ffmpeg

git checkout release/3.3


apt-get install -y libx264-dev


apt-get install -y  nasm


For compiling and using openh264  
   git clone    https://github.com/cisco/openh264 

   git checkout openh264v1.5
   
   cd openh264/
   
   make -j8
   
   make install

****************************************************************** to compile ffmpeg in static mode**************************************
// do not give prefix path

apt-get install -y  nasm

apt-get remove  libx264-dev

cd /workspace 

git clone    https://github.com/mirror/x264.git 

cd x264

./configure   --disable-opencl --enable-static

make -j8

make install


cd /export/views/video/ffmpeg

    ./configure --pkg-config-flags="--static" --libdir=/usr/local/lib --disable-shared --enable-static --enable-gpl --enable-pthreads --enable-nonfree  --enable-libfdk-aac    --enable-libx264 --enable-filters --enable-runtime-cpudetect --disable-lzma

debug

  cd /export/views/video/ffmpeg

  ./configure --pkg-config-flags="--static" --libdir=/usr/local/lib --disable-shared --enable-debug=2 --disable-optimizations --enable-static --enable-gpl --enable-pthreads --enable-nonfree  --enable-libfdk-aac    --enable-libx264 --enable-filters --enable-runtime-cpudetect --disable-lzma


enable logging of webrtc

chrome --enablewebrtc log


To compile webrtc
Download and Install the Chromium depot tools.

https://webrtc.github.io/webrtc-org/native-code/development/prerequisite-sw/

*Linux (Ubuntu/Debian)*
A script is provided for Ubuntu, which is unfortunately only available after your first gclient sync:

./build/install-build-deps.sh


https://webrtc.github.io/webrtc-org/native-code/development/

export PATH=/export/webrtc/depot_tools:$PATH

- mkdir webrtc-checkout
- cd webrtc-checkout
- fetch --nohooks webrtc
- gclient sync
- cd src
- git chekout m76
- gclient sync
- git apply provigil.patch


Note: Remove ffmpeg internal build from webrtc with rtc_use_h264=false



gn gen out/m75 --args='is_debug=true symbol_level=2 is_component_build=false is_clang=false rtc_include_tests=false rtc_use_h264=false rtc_enable_protobuf=false use_rtti=false use_custom_libcxx=false treat_warnings_as_errors=false use_ozone=true'



- Then build it:
- $ ninja -C out/m75


for release

gn gen out/m84_release --args='is_debug=false symbol_level=0 is_component_build=false is_clang=false rtc_include_tests=false rtc_use_h264=true rtc_enable_protobuf=false use_rtti=true use_custom_libcxx=false treat_warnings_as_errors=false use_ozone=true'

ninja -C out/m84_release










for nvidia hardware encoding

 gn gen out/m85 --args='is_debug=true symbol_level=2 is_component_build=false is_clang=false rtc_include_tests=false rtc_use_h264=true rtc_enable_protobuf=false use_rtti=true use_custom_libcxx=false treat_warnings_as_errors=false use_ozone=true proprietary_codecs=true ffmpeg_branding="Chrome"'


ninja -C out/m85/ webrtc


valgrind --leak-check=full   --show-leak-kinds=all  --track-origins=yes  ./runWebrtc  >& /var/tmp/leak.txt

ninja -C out/m75 webrtc


 ./configure --pkg-config-flags="--static" --libdir=/usr/local/lib --disable-shared --enable-debug=2 --disable-optimizations --enable-static --enable-gpl --enable-pthreads --enable-nonfree  --enable-libfdk-aac  --enable-nonfree --enable-libfdk-aac --enable-nvenc --enable-cuda   --enable-cuda-nvcc --enable-libx264 --enable-filters --enable-runtime-cpudetect --disable-lzma --disable-vdpau
